const User = require('../models/User');
const Course = require('../models/Course')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// checking if the email exists in the database
module.exports.checkEmailExists = (reqBoody) => {
	
	return User.find({email:reqBoody.email}).then(result => {
		
		if(result.length > 0){
			return true
		
		// no duplicate email found
		// USer is not yet registered in our database 
		} else{
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync(<dataToBeHash>,<saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		} else{
			return user;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	
	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false;
		} else{

			// compareSync(dataFromReqBody, encryptedDataFromDatabase)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else{
				return false;
			}
		}
	})
}

module.exports.userDetails = (reqBody) => {
	return User.findOne({_id:reqBody._id}).then(result => {
		
		if(result == null){
			return false
		} else{

			let displayDetails = {
				firstName: result.firstName,
				lastName: result.lastName,
				email: result.email,
				password: [],
				isAdmin: result.isAdmin,
				mobileNo: result.mobileNo,
				enrollments: result.enrollments,
				__v: result.__v
			}
			return displayDetails;
		}
	})
}

// enroll user to a course
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user,error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// add the userId in the course's enorllees array
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

	// condition that will check if the user and course documents have been updated
	if(isUserUpdated && isCourseUpdated){
		// enrollment successful
		return true;
	} else{
		return false;
	}
}