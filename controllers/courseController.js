const Course = require('../models/Course')
const User = require('../models/User')


module.exports.addCourse = (admin, reqBody) => {
	
	return User.findOne({isAdmin: admin.isAdmin}).then(result => {
		if(!admin.isAdmin){
			return false
		} else{
		
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newCourse.save().then((course, error) => {
				if(error){
					return false
				} else{
					return true
				}
			})
		}
	})
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result =>{
			return result;
	})
}

// retrieve all active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

// retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// update course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	/*
		Syntax:
			findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

// archive course
module.exports.archiveCourse = (reqParams, reqBody) => {

		let archive = {
			isActive: reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archive).then((course,error) => {
			if (error){
				return false
			} else{
				return true
			}
		})
}