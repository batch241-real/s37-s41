const express = require('express');

const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// route for checking if the users's email already exist in our database
router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// route for user registration
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


// route for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// route for user details
router.post('/details', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	userController.userDetails({_id: userData.id}).then(resultFromController => res.send(resultFromController))
})

module.exports = router;