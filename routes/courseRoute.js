const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const userController = require('../controllers/userController')
const auth = require('../auth');


// route for creating a course
router.post('/addCourse', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse({isAdmin:userData.isAdmin}, req.body).then(resultFromController => res.send(resultFromController));
})


// route for retrieving all the courses
router.get('/all', (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get('/active', (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})

// retrieve specific course
router.get('/:courseId', (req,res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

router.put('/:courseId', auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

router.patch('/:courseId/archive', auth.verify, (req,res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// route to enroll user to a course
router.post('/enroll', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;